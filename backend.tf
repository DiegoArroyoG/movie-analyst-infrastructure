terraform {
  backend "s3" {
    bucket = "diego-rampup-state"
    key    = "ramp-up/terraform.tfstate"
    region = "us-west-2"
  }
}
